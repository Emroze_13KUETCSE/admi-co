package com.androhungers.emroze.admi_co.versity_details;

public class VersityItem {
    private String name;
    private String id;
    private String exam;
    private String type;
    private String has_units;
    private String details;
    private String unit_details;
    private String form_fill_up;
    private String result;
    private String fee;
    private String status;
    private int resId;

    public VersityItem() {
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHas_units() {
        return has_units;
    }

    public void setHas_units(String has_units) {
        this.has_units = has_units;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getUnit_details() {
        return unit_details;
    }

    public void setUnit_details(String unit_details) {
        this.unit_details = unit_details;
    }

    public String getForm_fill_up() {
        return form_fill_up;
    }

    public void setForm_fill_up(String form_fill_up) {
        this.form_fill_up = form_fill_up;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }
}
