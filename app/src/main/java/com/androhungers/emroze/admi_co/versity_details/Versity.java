package com.androhungers.emroze.admi_co.versity_details;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androhungers.emroze.admi_co.R;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Versity extends AppCompatActivity {

    public static String PREFS_NAME = "Admi-Co";
    SharedPreferences preference;
    SharedPreferences.Editor editor;
    String baseUrl;
    RequestQueue requestQueue;
    RelativeLayout view;


    CardView cardView;
    boolean isSearched = false;
    EditText et;
    FloatingActionButton fab_search;
    RelativeLayout bg;
    String searchText="",searchField = "name",type = "general";

    RecyclerView recyclerView;
    VersityAdapter versityAdapter;
    ArrayList<VersityItem> versityItemArrayList = new ArrayList<>();
    ArrayList<VersityItem> versityItems = new ArrayList<>();
    String responseResult="";


    int[] imageId ={R.drawable.pic1, R.drawable.pic6, R.drawable.pic3, R.drawable.pic4, R.drawable.pic5};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_versity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        type = getIntent().getStringExtra("type");

        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        System.out.println(preference.getString("api_key",""));
        baseUrl = getString(R.string.base_url);
        requestQueue = Volley.newRequestQueue(this);

        view = findViewById(R.id.view);
        view.setVisibility(View.GONE);

        if(type.equals("general")){
            getSupportActionBar().setTitle("General University");
        }
        else if(type.equals("science")){
            getSupportActionBar().setTitle("Science & Technology");
        }
        else if(type.equals("engineering")){
            getSupportActionBar().setTitle("Engineering University");
        }
        else if(type.equals("medical")){
            getSupportActionBar().setTitle("Medical University");
        }
        recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(Versity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager);

        versityAdapter = new VersityAdapter(versityItems,getApplicationContext());
        versityAdapter.setListener(listener);
        recyclerView.setAdapter(versityAdapter);

        et = findViewById(R.id.et);
        cardView = findViewById(R.id.rl_search);
        fab_search = findViewById(R.id.fab);
        bg = findViewById(R.id.bg);


        fab_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSearched) {
                    isSearched = false;
                    cardView.animate().translationY(convertDpToPixel(-95, getApplicationContext()));
                    //cardView.setVisibility(View.INVISIBLE);
                    hideKeyboardFrom(getApplicationContext(), getCurrentFocus());
                    bg.setVisibility(View.INVISIBLE);
                } else {
                    isSearched = true;
                    cardView.setVisibility(View.VISIBLE);
                    et.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                    cardView.animate().translationY(0);

                    bg.setVisibility(View.VISIBLE);
                }
            }
        });

        bg.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (isSearched) {
                        isSearched = false;
                        cardView.animate().translationY(convertDpToPixel(-95, getApplicationContext()));
                        //cardView.setVisibility(View.INVISIBLE);
                        hideKeyboardFrom(getApplicationContext(), getCurrentFocus());
                        bg.setVisibility(View.INVISIBLE);
                    }
                }
                return true;
            }
        });

        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                searchText = String.valueOf(charSequence);


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        httpGetVersity();

    }

    public static float convertDpToPixel(float dp, Context context) {
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (NullPointerException e) {

        }

    }

    VersityAdapter.OnCarItemClickListener listener = new VersityAdapter.OnCarItemClickListener() {
        @Override
        public void onLongClick(int position, VersityItem item) {
            Intent intent = new Intent(Versity.this, VersityDetails.class);
            intent.putExtra("name",item.getName());
            intent.putExtra("data",responseResult);
            startActivity(intent);
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    public void httpGetVersity(){

        String url = baseUrl+"admi_co/v2/get_versity";
        view.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        view.setVisibility(View.GONE);
                        Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  RESPONSE ====================    "+response);
                        try {
                            //JSONObject object = new JSONObject(response);
                            JSONArray jsonArray = new JSONArray(response);
                            responseResult = response;
                            for(int i =0 ; i < jsonArray.length(); i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                String id = jsonObject.getString("id");
                                String name = jsonObject.getString("name");
                                String details = jsonObject.getString("details");
                                String has_units = jsonObject.getString("has_units");
                                String unit_details = jsonObject.getString("unit_details");
                                String form_fill_up = jsonObject.getString("form_fill_up");
                                String exam = jsonObject.getString("exam");
                                String result = jsonObject.getString("result");
                                String fee = jsonObject.getString("fee");
                                String status = jsonObject.getString("status");


                                VersityItem versityItem = new VersityItem();

                                versityItem.setId(id);
                                versityItem.setName(name);
                                versityItem.setDetails(details);
                                versityItem.setHas_units(has_units);
                                versityItem.setUnit_details(unit_details);
                                versityItem.setForm_fill_up(form_fill_up);
                                versityItem.setExam(exam);
                                versityItem.setResult(result);
                                versityItem.setFee(fee);
                                versityItem.setStatus(status);

                                Random rand = new Random();

                                int n = rand.nextInt(5) + 0;
                                versityItem.setResId(imageId[n]);

                                versityItemArrayList.add(versityItem);
                                System.out.println(">>>>>>>>>>>>>>>>   "+name);
                                System.out.println(">>>>>>>>>>>>>>>>   "+has_units);
                                boolean isExists = false;
                                for(VersityItem item:versityItems){
                                    if(item.getName().toUpperCase().equals(versityItem.getName().toUpperCase())){
                                        isExists = true;
                                        break;
                                    }
                                }
                                if(!isExists){
                                    System.out.println(">>>>>>>>>>>>>>>>   "+name);
                                    System.out.println(">>>>>>>>>>>>>>>>   "+has_units);
                                    versityItems.add(versityItem);
                                }

                            }

                           versityAdapter.setItemList(versityItems);
                            versityAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        view.setVisibility(View.GONE);
                        System.out.println(error.toString());
                        Toast.makeText(Versity.this, "Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", preference.getString("api_key",""));
                return params;
            }

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type", type);
                return params;
            }
            @Override
            protected com.android.volley.Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10*1000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        stringRequest.setShouldCache(false);

        requestQueue.add(stringRequest);


    }

}
