package com.androhungers.emroze.admi_co.versity_details;

import java.util.ArrayList;

public class UnitItem {
    String name;
    String fee;
    boolean isSelected = false;
    ArrayList<DetailsItem> detailsItems;

    public UnitItem() {
    }

    public UnitItem(String name, boolean isSelected) {
        this.name = name;
        this.isSelected = isSelected;
    }

    public UnitItem(String name, String fee, boolean isSelected) {
        this.name = name;
        this.fee = fee;
        this.isSelected = isSelected;
    }

    public ArrayList<DetailsItem> getDetailsItems() {
        return detailsItems;
    }

    public void setDetailsItems(ArrayList<DetailsItem> detailsItems) {
        this.detailsItems = detailsItems;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
