package com.androhungers.emroze.admi_co.versity_details;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androhungers.emroze.admi_co.R;

import java.util.ArrayList;

public class UnitAdapter extends RecyclerView.Adapter<UnitAdapter.CarsViewHolder>{

    public ArrayList<UnitItem> itemArrayList;
    Context context;
    private OnCarItemClickListener listener;

    public interface OnCarItemClickListener {

        /**
         * The method declaration for user selected. This method will be fired
         * when user click on check/uncheck the checkbox on the list item.
         *
         * @param position
         * @param item
         */
        public void onLongClick(int position, UnitItem item);

    }
    public void setListener(UnitAdapter.OnCarItemClickListener listener) {
        this.listener = listener;
    }

    public UnitAdapter(ArrayList<UnitItem> items, Context context){
        this.itemArrayList= items;
        this.context = context;
    }

    public void setItemList(ArrayList<UnitItem> list) {
        itemArrayList = list;
    }

    @Override
    public CarsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.unit_item, parent, false);
        CarsViewHolder gvh = new CarsViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final UnitAdapter.CarsViewHolder holder, final int position) {

        final UnitItem item = itemArrayList.get(position);

        holder.name.setText(item.getName());

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onLongClick(position,item);
            }
        });

        if(item.isSelected()){
            holder.name.setBackgroundResource(R.drawable.border7);
            holder.name.setTextColor(Color.WHITE);
        }
        else {
            holder.name.setBackgroundResource(R.drawable.border8);
            holder.name.setTextColor(Color.BLACK);
        }



    }

    @Override
    public int getItemCount() {
        if (itemArrayList != null) {
            return itemArrayList.size();
        } else {
            return 0;
        }
    }

    public class CarsViewHolder extends RecyclerView.ViewHolder {
        TextView name;


        public CarsViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.tv_name);

            Typeface type1 = Typeface.createFromAsset(context.getAssets(),context.getString(R.string.font));

            name.setTypeface(type1);

        }
    }
}


