package com.androhungers.emroze.admi_co.versity_details;

public class DetailsItem {
    String title;
    String details;
    boolean isExpanded = false;

    public DetailsItem() {
    }

    public DetailsItem(String title, String details, boolean isExpanded) {
        this.title = title;
        this.details = details;
        this.isExpanded = isExpanded;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }
}
