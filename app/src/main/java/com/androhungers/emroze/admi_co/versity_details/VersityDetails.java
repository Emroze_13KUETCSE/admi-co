package com.androhungers.emroze.admi_co.versity_details;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androhungers.emroze.admi_co.MainActivity;
import com.androhungers.emroze.admi_co.OnBoardItem;
import com.androhungers.emroze.admi_co.OnBoard_Adapter;
import com.androhungers.emroze.admi_co.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class VersityDetails extends AppCompatActivity {

    String name,id,units;

    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    private ViewPager onboard_pager;
    private OnBoard_Adapter mAdapter;
    int previous_pos=0;
    ArrayList<OnBoardItem> onBoardItems=new ArrayList<>();
    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 900;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 1500; // time in milliseconds between successive task executions.


    RecyclerView recyclerView;
    ArrayList<UnitItem> unitsItems = new ArrayList<>();
    UnitAdapter unitAdapter;


    RecyclerView recyclerView2;
    ArrayList<DetailsItem> detailsItems = new ArrayList<>();
    DetailsAdapter detailsAdapter;

    ArrayList<VersityItem> versityItemArrayList = new ArrayList<>();


    String response;

    TextView tvDetails,tvFormFillUp,tvExam,tvResult,tvName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_versity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvDetails = findViewById(R.id.tv_details);
        tvFormFillUp = findViewById(R.id.tv_form_fill_up);
        tvExam = findViewById(R.id.tv_exam);
        tvResult = findViewById(R.id.tv_result);
        tvName = findViewById(R.id.tv_name);

        name = getIntent().getStringExtra("name");
        response = getIntent().getStringExtra("data");
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(response);

            for(int i =0 ; i < jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String id = jsonObject.getString("id");
                String name = jsonObject.getString("name");

                if(name.equals(this.name)){
                    String details = jsonObject.getString("details");
                    String has_units = jsonObject.getString("has_units");
                    String unit_details = jsonObject.getString("unit_details");
                    String form_fill_up = jsonObject.getString("form_fill_up");
                    String exam = jsonObject.getString("exam");
                    String result = jsonObject.getString("result");

                    VersityItem profileItem = new VersityItem();

                    profileItem.setId(id);
                    profileItem.setName(name);
                    profileItem.setDetails(details);
                    profileItem.setHas_units(has_units);
                    profileItem.setUnit_details(unit_details);
                    profileItem.setForm_fill_up(form_fill_up);
                    profileItem.setExam(exam);
                    profileItem.setResult(result);
                    versityItemArrayList.add(profileItem);

                    UnitItem unitItem = new UnitItem(has_units,false);

                    JSONArray jsonArray1 = new JSONArray(unit_details);
                    ArrayList<DetailsItem> detailsItems = new ArrayList<>();

                    for(int j = 0; j < jsonArray1.length(); j++){
                        JSONObject jj = jsonArray1.getJSONObject(j);
                        detailsItems.add(new DetailsItem(jj.getString("value"),jj.getString("data"),false));
                    }

                    unitItem.setDetailsItems(detailsItems);
                    unitItem.setFee(jsonObject.getString("fee"));
                    unitsItems.add(unitItem);

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(name);

        int[] imageId ={R.drawable.pic1, R.drawable.pic6, R.drawable.pic3, R.drawable.pic4, R.drawable.pic5};

        for(int i=0;i<imageId.length;i++)
        {
            OnBoardItem item=new OnBoardItem();
            item.setImageID(imageId[i]);
            onBoardItems.add(item);
        }

        onboard_pager = (ViewPager) findViewById(R.id.pager_introduction);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);

        mAdapter = new OnBoard_Adapter(this,onBoardItems);
        onboard_pager.setAdapter(mAdapter);
        onboard_pager.setCurrentItem(0);
        onboard_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                currentPage=position;
                // Change the current position intimation

                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(VersityDetails.this, R.drawable.non_selected_item_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(VersityDetails.this, R.drawable.selected_item_dot));


                int pos=position+1;


                previous_pos=pos;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setUiPageViewController();

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == 5 ) {
                    currentPage = 0;
                }
                onboard_pager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer .schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);


        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.hasFixedSize();
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(VersityDetails.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager);


        unitsItems.get(0).setSelected(true);
        unitAdapter = new UnitAdapter(unitsItems,getApplicationContext());
        unitAdapter.setListener(listener);
        recyclerView.setAdapter(unitAdapter);
        unitAdapter.notifyDataSetChanged();
        detailsItems = unitsItems.get(0).getDetailsItems();


        recyclerView2 = findViewById(R.id.recycler_view2);
        recyclerView2.hasFixedSize();
        LinearLayoutManager horizontalLayoutManager2 = new LinearLayoutManager(VersityDetails.this, LinearLayoutManager.VERTICAL, false);
        recyclerView2.setLayoutManager(horizontalLayoutManager2);


        detailsAdapter = new DetailsAdapter(unitsItems.get(0).getDetailsItems(),getApplicationContext());
        detailsAdapter.setListener(listener2);
        recyclerView2.setAdapter(detailsAdapter);
        detailsAdapter.notifyDataSetChanged();

        tvDetails.setText(versityItemArrayList.get(0).getDetails());
        tvFormFillUp.setText(versityItemArrayList.get(0).getForm_fill_up());
        tvExam.setText(versityItemArrayList.get(0).getExam());
        tvResult.setText(versityItemArrayList.get(0).getResult());
        tvName.setText(name);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VersityDetails.this, FormFillUp.class);
                intent.putExtra("name",name);
                intent.putExtra("data",response);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    DetailsAdapter.OnCarItemClickListener listener2 = new DetailsAdapter.OnCarItemClickListener() {
        @Override
        public void onLongClick(int position, DetailsItem item) {
            if(item.isExpanded()){
                detailsItems.get(position).setExpanded(false);
            }
            else {
                detailsItems.get(position).setExpanded(true);
            }

            detailsAdapter.setItemList(detailsItems);
            detailsAdapter.notifyItemChanged(position);
        }
    };

    UnitAdapter.OnCarItemClickListener listener = new UnitAdapter.OnCarItemClickListener() {
        @Override
        public void onLongClick(int position, UnitItem item) {
            int i =0;
            for(UnitItem item1 : unitsItems){
                if(i == position){
                    unitsItems.get(i).setSelected(true);
                    tvDetails.setText(versityItemArrayList.get(i).getDetails());
                    tvFormFillUp.setText(versityItemArrayList.get(i).getForm_fill_up());
                    tvExam.setText(versityItemArrayList.get(i).getExam());
                    tvResult.setText(versityItemArrayList.get(i).getResult());

                    detailsItems = unitsItems.get(i).getDetailsItems();
                    detailsAdapter.setItemList(unitsItems.get(i).getDetailsItems());
                    detailsAdapter.notifyDataSetChanged();
                }
                else {
                    unitsItems.get(i).setSelected(false);
                }
                ++i;
            }

            unitAdapter.setItemList(unitsItems);
            unitAdapter.notifyDataSetChanged();

        }
    };

    private void setUiPageViewController() {

        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(VersityDetails.this, R.drawable.non_selected_item_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(convertDpToPixel(5,this), 0, 5, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(VersityDetails.this, R.drawable.selected_item_dot));
    }

    public static int convertDpToPixel(int dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = dp * ((int)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }


}
