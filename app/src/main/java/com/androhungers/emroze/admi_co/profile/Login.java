package com.androhungers.emroze.admi_co.profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androhungers.emroze.admi_co.MainActivity;
import com.androhungers.emroze.admi_co.R;
import com.androhungers.emroze.admi_co.signIn.CreateAccount;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    public static String PREFS_NAME = "Admi-Co";
    SharedPreferences preference;
    SharedPreferences.Editor editor;
    String baseUrl;
    RequestQueue requestQueue;
    ProgressBar view;

    TextView tvNext;
    EditText etPhone;
    ProfileDB profileDB;
    LinearLayout  llCreateAccount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Login");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //


        profileDB = new ProfileDB(getApplicationContext());

        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        System.out.println(preference.getString("api_key",""));
        baseUrl = getString(R.string.base_url);
        requestQueue = Volley.newRequestQueue(this);

        view =findViewById(R.id.progress_bar);
        tvNext = findViewById(R.id.tv_next);
        etPhone = findViewById(R.id.et_phone);

        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etPhone.getText().toString().isEmpty()){
                    Toast.makeText(Login.this, "Please enter phone number", Toast.LENGTH_SHORT).show();
                }else {
                    httpLogin(etPhone.getText().toString());
                }
            }
        });

        llCreateAccount = findViewById(R.id.ll_sign_up);
        llCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), CreateAccount.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }


    public void httpLogin(final String phone){

        String url = baseUrl+"admi_co/v2/login";
        view.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        view.setVisibility(View.GONE);
                        Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  RESPONSE ====================    "+response);
                        try {
                            JSONObject object = new JSONObject(response);
                            boolean ss = object.getBoolean("success");
                            if(ss){
                                ProfileItem profileItem = new ProfileItem();

                                JSONObject jsonObject_ssc = new JSONObject(object.getJSONObject("data").getString("ssc"));
                                try {
                                    profileItem.setSsc_board(jsonObject_ssc.getString("board"));;
                                    profileItem.setSsc_gpa(jsonObject_ssc.getString("gpa"));
                                    profileItem.setSsc_regi(jsonObject_ssc.getString("regi"));
                                    profileItem.setSsc_roll(jsonObject_ssc.getString("roll"));
                                    profileItem.setSsc_year(jsonObject_ssc.getString("year"));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                JSONObject jsonObject_hsc = new JSONObject(object.getJSONObject("data").getString("hsc"));
                                try {
                                    profileItem.setHsc_board(jsonObject_hsc.getString("board"));
                                    profileItem.setHsc_gpa(jsonObject_hsc.getString("gpa"));
                                    profileItem.setHsc_regi(jsonObject_hsc.getString("regi"));
                                    profileItem.setHsc_roll(jsonObject_hsc.getString("roll"));
                                    profileItem.setHsc_year(jsonObject_hsc.getString("year"));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                profileItem.setId(object.getJSONObject("data").getString("id"));
                                profileItem.setApiKey(object.getJSONObject("data").getString("api_key"));
                                profileItem.setName(object.getJSONObject("data").getString("name"));
                                profileItem.setPassword(object.getJSONObject("data").getString("password"));
                                profileItem.setPpUrl(object.getJSONObject("data").getString("pp_url"));
                                profileItem.setPhone(object.getJSONObject("data").getString("phone"));
                                profileItem.setSignatureUrl(object.getJSONObject("data").getString("sign_url"));

                                editor.putString("api_key",object.getJSONObject("data").getString("api_key"));
                                editor.putString("name",profileItem.getName());
                                editor.putString("phone",profileItem.getPhone());
                                editor.putString("id",profileItem.getId());
                                editor.putString("pp_url",profileItem.getPpUrl());
                                editor.putString("sign_url",profileItem.getSignatureUrl());
                                editor.putString("sign_in","true");
                                editor.commit();

                                profileDB.insertToProfileList(profileItem);
                                finish();
                                Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                            else {
                                Toast.makeText(Login.this, "Please try again", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        view.setVisibility(View.GONE);
                        Toast.makeText(Login.this, "Please try again. No Internet", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", preference.getString("api_key",""));
                return params;
            }

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone", phone);

                return params;
            }
            @Override
            protected com.android.volley.Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10*1000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        stringRequest.setShouldCache(false);

        requestQueue.add(stringRequest);


    }

}
