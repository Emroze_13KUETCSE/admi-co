package com.androhungers.emroze.admi_co.form;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androhungers.emroze.admi_co.CustomDialog;
import com.androhungers.emroze.admi_co.R;
import com.androhungers.emroze.admi_co.versity_details.FormFillUp;
import com.androhungers.emroze.admi_co.versity_details.Versity;
import com.androhungers.emroze.admi_co.versity_details.VersityAdapter;
import com.androhungers.emroze.admi_co.versity_details.VersityItem;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.androhungers.emroze.admi_co.Constants.ADMIT_CARD_OK;
import static com.androhungers.emroze.admi_co.Constants.PAYMENT_NOT_CONFIRM;

public class FormListActivity extends AppCompatActivity {

    public static String PREFS_NAME = "Admi-Co";
    SharedPreferences preference;
    SharedPreferences.Editor editor;
    String baseUrl;
    RequestQueue requestQueue;
    RelativeLayout view;

    RecyclerView recyclerView;
    FormAdapter formAdapter;
    ArrayList<FormItem> versityItemArrayList = new ArrayList<>();

    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("FormList");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        System.out.println(preference.getString("api_key",""));
        baseUrl = getString(R.string.base_url);
        requestQueue = Volley.newRequestQueue(this);

        view = findViewById(R.id.view);
        view.setVisibility(View.GONE);


        id = preference.getString("id","");

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(FormListActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager);

        formAdapter = new FormAdapter(versityItemArrayList,getApplicationContext());
        formAdapter.setListener(listener);
        recyclerView.setAdapter(formAdapter);


        httpGetList(id);

    }

    FormAdapter.OnCarItemClickListener listener = new FormAdapter.OnCarItemClickListener() {
        @Override
        public void onLongClick(int position, final FormItem item) {
            if(item.getStatus().equals(PAYMENT_NOT_CONFIRM)){
                CustomDialog customDialog2 = new CustomDialog();
                customDialog2.setDialog(FormListActivity.this,getApplicationContext(),R.layout.tx_dialog);
                final Dialog dialogView2 = customDialog2.getView();
                dialogView2.show();

                dialogView2.findViewById(R.id.ll_ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        EditText et_txId = dialogView2.findViewById(R.id.et_tx_id);
                        EditText et_txNumber = dialogView2.findViewById(R.id.et_tx_number);

                        if(et_txId.getText().toString().isEmpty() || et_txNumber.getText().toString().isEmpty()){
                            Toast.makeText(FormListActivity.this, "Enter TxId/TxNumber", Toast.LENGTH_SHORT).show();
                        }else {
                            dialogView2.hide();
                            httpPutTx(item.getId(),et_txId.getText().toString(),et_txNumber.getText().toString());
                        }

                    }
                });
            }

            if(item.getStatus().equals(ADMIT_CARD_OK)){
                CustomDialog customDialog2 = new CustomDialog();
                customDialog2.setDialog(FormListActivity.this,getApplicationContext(),R.layout.payment_confirm_dialog);
                final Dialog dialogView2 = customDialog2.getView();

                TextView tv = dialogView2.findViewById(R.id.tv_info);
                tv.setText(item.getAdmitUrl());

                dialogView2.show();

                dialogView2.findViewById(R.id.ll_ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        dialogView2.hide();



                    }
                });
            }
        }
    };

    public void httpGetList(final String id){
        String url = baseUrl+"admi_co/v2/get_form";
        view.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        view.setVisibility(View.GONE);
                        Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  RESPONSE ====================    "+response);

                        try {

                            JSONArray jsonArray = new JSONArray(response);

                            versityItemArrayList.clear();
                            for(int i =0 ; i < jsonArray.length(); i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);


                                String id = jsonObject.getString("id");
                                String versity_name = jsonObject.getString("versity_name");
                                String unit_name = jsonObject.getString("unit_name");
                                String tx_phone = jsonObject.getString("tx_phone");
                                String tx_id = jsonObject.getString("tx_id");
                                String student_id = jsonObject.getString("student_id");
                                String student_number = jsonObject.getString("student_number");
                                String admit_url = jsonObject.getString("admit_url");
                                String status = jsonObject.getString("status");
                                String payment_amount = jsonObject.getString("payment_amount");
                                String date_info = jsonObject.getString("date_info");



                                FormItem formItem = new FormItem();

                                formItem.setAdmitUrl(admit_url);
                                formItem.setId(id);
                                formItem.setVersityName(versity_name);
                                formItem.setUnit(unit_name);
                                formItem.setTxId(tx_id);
                                formItem.setTxPhone(tx_phone);
                                formItem.setStdId(student_id);
                                formItem.setStdNumber(student_number);
                                formItem.setPaymentAmount(payment_amount);
                                formItem.setStatus(status);
                                formItem.setDate(date_info);



                                versityItemArrayList.add(formItem);


                            }

                            formAdapter.setItemList(versityItemArrayList);
                            formAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        view.setVisibility(View.GONE);
                        System.out.println(error.toString());
                        Toast.makeText(FormListActivity.this, "Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", preference.getString("api_key",""));
                return params;
            }

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", id);
                return params;
            }
            @Override
            protected com.android.volley.Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10*1000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        stringRequest.setShouldCache(false);

        requestQueue.add(stringRequest);

    }


    public void httpPutTx(final String fid, final String txId, final String txNumber){
        String url = baseUrl+"admi_co/v2/update_tx";
        view.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        view.setVisibility(View.GONE);
                        Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  RESPONSE ====================    "+response);

                        httpGetList(id);
                        //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        view.setVisibility(View.GONE);
                        System.out.println(error.toString());
                        Toast.makeText(FormListActivity.this, "Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", preference.getString("api_key",""));
                return params;
            }

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", fid);
                params.put("tx_id", txId);
                params.put("tx_phone", txNumber);
                params.put("status", "1");

                return params;
            }
            @Override
            protected com.android.volley.Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10*1000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        stringRequest.setShouldCache(false);

        requestQueue.add(stringRequest);

    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}
