package com.androhungers.emroze.admi_co;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.androhungers.emroze.admi_co.form.FormListActivity;
import com.androhungers.emroze.admi_co.profile.DialogProfileAdapter;
import com.androhungers.emroze.admi_co.profile.Login;
import com.androhungers.emroze.admi_co.profile.ProfileDB;
import com.androhungers.emroze.admi_co.profile.ProfileDetails;
import com.androhungers.emroze.admi_co.profile.ProfileItem;
import com.androhungers.emroze.admi_co.versity_details.Versity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nex3z.notificationbadge.NotificationBadge;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity{

    public static String PREFS_NAME = "Admi-Co";
    SharedPreferences preference;
    SharedPreferences.Editor editor;
    String baseUrl;

    private DrawerLayout drawer;
    private ImageView toggle;
    TextView tvProfileName;

    Typeface type,type2;

    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    private ViewPager onboard_pager;
    private OnBoard_Adapter mAdapter;
    int previous_pos=0;
    ArrayList<OnBoardItem> onBoardItems=new ArrayList<>();
    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 900;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 1500; // time in milliseconds between successive task executions.


    NotificationBadge notificationBadge1,noti_general,noti_eng,noti_medical,noti_sci,noti_agr,noti_fav;

    Dialog dialogView;
    CustomDialog customDialog;
    RecyclerView profileList;
    ProfileDB profileDB;

    ArrayList<ProfileItem> profileItems = new ArrayList<>();
    DialogProfileAdapter dialogProfileAdapter;

    CircleImageView imgProfile,imgPP2;

    LinearLayout llAdd,llGeneral,llMyForm,llNotice,llScience,llEngineering,llMedical,llAgriculture;

    String name="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        baseUrl = getString(R.string.base_url);
        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        name = preference.getString("name","");
        if(preference.getString("api_key","").equals("")){
            name = "Admi-Co";
            editor.putString("api_key","pGYBd52nZZrR_7UqpxmKD2_A9ZX2QAzRSr");
            editor.commit(); }
        if(preference.getString("sign_in","").equals("true")){
            name = preference.getString("name","");
        }
        else {
            name = "Admi-Co";
        }
        profileDB = new ProfileDB(getApplicationContext());

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        tvProfileName = findViewById(R.id.tv_name);

        toggle = findViewById(R.id.img);

        toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        imgProfile = findViewById(R.id.img_profile);
        imgPP2 = findViewById(R.id.img_pp_side);





        type = Typeface.createFromAsset(getAssets(),getString(R.string.font));
        type2 = Typeface.createFromAsset(getAssets(),getString(R.string.font));

        tvProfileName.setTypeface(type);



        ListView rvNumbers = (ListView) findViewById(R.id.listView);

        String [] numbers = {"Helpline" ,"Profile", "About Us","Information",};

        ItemArrayAdapter itemArrayAdapter = new ItemArrayAdapter(this, R.layout.list_item, numbers,drawer);
        rvNumbers.setAdapter(itemArrayAdapter);


//image set
        int[] imageId ={R.drawable.pic1, R.drawable.pic6, R.drawable.pic3, R.drawable.pic4, R.drawable.pic5};

        for(int i=0;i<imageId.length;i++)
        {
           OnBoardItem item=new OnBoardItem();
            item.setImageID(imageId[i]);
            onBoardItems.add(item);
        }

        onboard_pager = (ViewPager) findViewById(R.id.pager_introduction);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);

        mAdapter = new OnBoard_Adapter(this,onBoardItems);
        onboard_pager.setAdapter(mAdapter);
        onboard_pager.setCurrentItem(0);
        onboard_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            //main page image gula change
            public void onPageSelected(int position) {

                currentPage=position;
                // Change the current position intimation

                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.non_selected_item_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.selected_item_dot));


                int pos=position+1;


                previous_pos=pos;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setUiPageViewController();

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == 5 ) {
                    currentPage = 0;
                }
                onboard_pager.setCurrentItem(currentPage++, true);
            }
        };
//time set
        timer = new Timer(); // This will create a new Thread
        timer .schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);



        notificationBadge1 = findViewById(R.id.badge);

        noti_general = findViewById(R.id.badge_general);
        noti_eng = findViewById(R.id.badge_engineering);
        noti_medical = findViewById(R.id.badge_medical);
        noti_sci = findViewById(R.id.badge_science);
        noti_agr = findViewById(R.id.badge_agriculture);


        notificationBadge1.setNumber(20,true);

        noti_general.setNumber(3,true);
        noti_eng.setNumber(1,true);
        noti_medical.setNumber(4,true);
        noti_sci.setNumber(10,true);
        noti_agr.setNumber(2,true);

        customDialog = new CustomDialog();
        customDialog.setDialog(MainActivity.this,getApplicationContext(),R.layout.profile_list_dialog);
        dialogView = customDialog.getView();

        profileList = dialogView.findViewById(R.id.recycler_view);
        profileList.setHasFixedSize(true);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        profileList.setLayoutManager(horizontalLayoutManager);


        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profileItems = profileDB.getProfileList();

                dialogProfileAdapter = new DialogProfileAdapter(profileItems,getApplicationContext());
                dialogProfileAdapter.setItemList(profileItems);
                profileList.setAdapter(dialogProfileAdapter);
                dialogProfileAdapter.setListener(dialogItemListern);
                dialogProfileAdapter.notifyDataSetChanged();

                System.out.println(profileItems.size());

                customDialog.showDialog();
            }
        });

        llAdd = dialogView.findViewById(R.id.ll_add);

        llAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.hideDialog();
                Intent intent = new Intent(MainActivity.this, Login.class);
                startActivity(intent);
            }
        });

        llGeneral = findViewById(R.id.ll_general);

        llGeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Versity.class);
                intent.putExtra("type","general");
                startActivity(intent);
            }
        });

        llScience = findViewById(R.id.ll_science);

        llScience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Versity.class);
                intent.putExtra("type","science");
                startActivity(intent);
            }
        });


        llEngineering = findViewById(R.id.ll_engineering);

        llEngineering.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Versity.class);
                intent.putExtra("type","engineering");
                startActivity(intent);
            }
        });


        llMedical = findViewById(R.id.ll_medical);

        llMedical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Versity.class);
                intent.putExtra("type","medical");
                startActivity(intent);
            }
        });




        llMyForm = findViewById(R.id.ll_my_form);

        llMyForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FormListActivity.class);
                startActivity(intent);
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();

        tvProfileName.setText(preference.getString("name",""));

        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_pp2)
                .error(R.mipmap.ic_pp2)
                .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                .skipMemoryCache(true);

        System.out.println(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png");
        Glide.with(getApplicationContext())
                .applyDefaultRequestOptions(requestOptions)
                .load(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png")
                .into(imgProfile);

        RequestOptions requestOptions2 = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_pp)
                .error(R.mipmap.ic_pp)
                .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                .skipMemoryCache(true);

        System.out.println(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png");
        Glide.with(getApplicationContext())
                .applyDefaultRequestOptions(requestOptions2)
                .load(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png")
                .into(imgPP2);
    }

    DialogProfileAdapter.OnCarItemClickListener dialogItemListern = new DialogProfileAdapter.OnCarItemClickListener() {
        @Override
        public void onLongClick(int position, ProfileItem item) {
            Intent intent = new Intent(MainActivity.this, ProfileDetails.class);
            startActivity(intent);
        }

        @Override
        //profile item gula update hosse
        public void onClick(int position, ProfileItem profileItem) {
            editor.putString("api_key",profileItem.getApiKey());
            editor.putString("name",profileItem.getName());
            editor.putString("phone",profileItem.getPhone());
            editor.putString("id",profileItem.getId());
            editor.putString("sign_in","true");
            editor.commit();

            RequestOptions requestOptions = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.mipmap.ic_pp2)
                    .error(R.mipmap.ic_pp2)
                    .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                    .skipMemoryCache(true);

            System.out.println(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png");
            Glide.with(getApplicationContext())
                    .applyDefaultRequestOptions(requestOptions)
                    .load(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png")
                    .into(imgProfile);

            RequestOptions requestOptions2 = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.mipmap.ic_pp)
                    .error(R.mipmap.ic_pp)
                    .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                    .skipMemoryCache(true);

            System.out.println(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png");
            Glide.with(getApplicationContext())
                    .applyDefaultRequestOptions(requestOptions2)
                    .load(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png")
                    .into(imgPP2);

            name = preference.getString("name","");
            tvProfileName.setText(name);
            dialogProfileAdapter.notifyDataSetChanged();
        }
    };

    private void setUiPageViewController() {

        //***main page image dot er kaj
        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.non_selected_item_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(convertDpToPixel(5,this), 0, 5, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.selected_item_dot));
    }

    public static int convertDpToPixel(int dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = dp * ((int)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }


    @Override
    //navigation drawer open close
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




}
