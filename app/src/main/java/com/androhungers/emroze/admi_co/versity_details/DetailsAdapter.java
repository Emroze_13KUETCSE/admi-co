package com.androhungers.emroze.admi_co.versity_details;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androhungers.emroze.admi_co.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class DetailsAdapter extends RecyclerView.Adapter<DetailsAdapter.CarsViewHolder>{

    public ArrayList<DetailsItem> itemArrayList;
    Context context;
    private OnCarItemClickListener listener;

    public interface OnCarItemClickListener {

        /**
         * The method declaration for user selected. This method will be fired
         * when user click on check/uncheck the checkbox on the list item.
         *
         * @param position
         * @param item
         */
        public void onLongClick(int position, DetailsItem item);

    }
    public void setListener(DetailsAdapter.OnCarItemClickListener listener) {
        this.listener = listener;
    }

    public DetailsAdapter(ArrayList<DetailsItem> items, Context context){
        this.itemArrayList= items;
        this.context = context;
    }

    public void setItemList(ArrayList<DetailsItem> list) {
        itemArrayList = list;
    }

    @Override
    public CarsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.details_item, parent, false);
        CarsViewHolder gvh = new CarsViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final DetailsAdapter.CarsViewHolder holder, final int position) {

        final DetailsItem item = itemArrayList.get(position);

        holder.title.setText(item.getTitle());
        holder.details.setText(item.getDetails());

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onLongClick(position,item);
            }
        });

        if(item.isExpanded()){
            holder.expandableLayout.expand();
        }
        else {
            holder.expandableLayout.collapse();
        }



    }

    @Override
    public int getItemCount() {
        if (itemArrayList != null) {
            return itemArrayList.size();
        } else {
            return 0;
        }
    }

    public class CarsViewHolder extends RecyclerView.ViewHolder {
        TextView title,details;
        LinearLayout ll;
        ExpandableLayout expandableLayout;


        public CarsViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.tv_title);
            details = (TextView) view.findViewById(R.id.tv_details);
            ll = view.findViewById(R.id.ll);
            expandableLayout =  view.findViewById(R.id.expandable_layout);


        }
    }
}



