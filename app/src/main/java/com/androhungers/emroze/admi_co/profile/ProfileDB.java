package com.androhungers.emroze.admi_co.profile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class ProfileDB extends SQLiteOpenHelper {
    public static final String DBname = "admico_profile.db";
    public static final String TABLE_NAME = "profile";


    public ProfileDB(Context context) {
        super(context, DBname, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+TABLE_NAME+" (id INTEGER PRIMARY KEY AUTOINCREMENT,user_id TEXT,name TEXT,phone TEXT,api_key TEXT,password TEXT,ssc_year TEXT" +
                ",ssc_board TEXT,ssc_gpa TEXT,ssc_regi TEXT,ssc_roll TEXT,hsc_year TEXT,hsc_board TEXT,hsc_gpa TEXT" +
                ",hsc_regi TEXT,hsc_roll TEXT,ppUrl TEXT,signatureUrl TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    public Boolean insertToProfileList(ProfileItem item) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor rs = db.rawQuery("select * from "+TABLE_NAME+" WHERE user_id = '"+item.getId()+"'", null);
        rs.moveToFirst();
        boolean isExist = false;
        while (!rs.isAfterLast()) {

            isExist = true;

            break;
            //rs.moveToNext();
         }
        boolean b_result = false;

        if(isExist){

            ContentValues contentValues=new ContentValues();
            contentValues.put("name",item.getName());
            contentValues.put("phone",item.getPhone());
            contentValues.put("password",item.getPassword());
            contentValues.put("api_key",item.getApiKey());
            contentValues.put("ssc_year",item.getSsc_year());
            contentValues.put("ssc_board",item.getSsc_board());
            contentValues.put("ssc_gpa",item.getSsc_gpa());
            contentValues.put("ssc_regi",item.getSsc_regi());
            contentValues.put("ssc_roll",item.getSsc_roll());
            contentValues.put("hsc_year",item.getHsc_year());
            contentValues.put("hsc_board",item.getHsc_board());
            contentValues.put("hsc_gpa",item.getHsc_gpa());
            contentValues.put("hsc_regi",item.getHsc_regi());
            contentValues.put("hsc_roll",item.getHsc_roll());
            contentValues.put("ppUrl",item.getPpUrl());
            contentValues.put("signatureUrl",item.getSignatureUrl());

            db.update(TABLE_NAME,contentValues,"user_id = ?",new String[]{item.getId()});

            b_result = true;
        }
        else {
            ContentValues contentValues = new ContentValues();
            contentValues.put("user_id",item.getId());
            contentValues.put("name",item.getName());
            contentValues.put("phone",item.getPhone());
            contentValues.put("password",item.getPassword());
            contentValues.put("api_key",item.getApiKey());
            contentValues.put("ssc_year",item.getSsc_year());
            contentValues.put("ssc_board",item.getSsc_board());
            contentValues.put("ssc_gpa",item.getSsc_gpa());
            contentValues.put("ssc_regi",item.getSsc_regi());
            contentValues.put("ssc_roll",item.getSsc_roll());
            contentValues.put("hsc_year",item.getHsc_year());
            contentValues.put("hsc_board",item.getHsc_board());
            contentValues.put("hsc_gpa",item.getHsc_gpa());
            contentValues.put("hsc_regi",item.getHsc_regi());
            contentValues.put("hsc_roll",item.getHsc_roll());
            contentValues.put("ppUrl",item.getPpUrl());
            contentValues.put("signatureUrl",item.getSignatureUrl());

            long result = db.insert(TABLE_NAME, null, contentValues);

            if (result == -1)
                b_result =  false;
            else
                b_result =  true;
        }

        return b_result;

    }

    public Boolean update_a_value(String id, String task_id) {
        SQLiteDatabase db = this.getReadableDatabase();

        ContentValues contentValues=new ContentValues();

        contentValues.put("task_id",task_id);

        db.update(TABLE_NAME,contentValues,"task_id = ?",new String[]{id});


        return true;

    }



    public ArrayList getProfileList() {
        ArrayList<ProfileItem> arraylist = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor rs = db.rawQuery("select * from "+TABLE_NAME, null);
        rs.moveToFirst();
        while (!rs.isAfterLast()) {

            ProfileItem itemDB = new ProfileItem();

            itemDB.setId(rs.getString(rs.getColumnIndex("user_id")));
            itemDB.setName(rs.getString(rs.getColumnIndex("name")));
            itemDB.setPhone(rs.getString(rs.getColumnIndex("phone")));
            itemDB.setPassword(rs.getString(rs.getColumnIndex("password")));
            itemDB.setApiKey(rs.getString(rs.getColumnIndex("api_key")));
            itemDB.setSsc_year(rs.getString(rs.getColumnIndex("ssc_year")));
            itemDB.setSsc_board(rs.getString(rs.getColumnIndex("ssc_board")));
            itemDB.setSsc_gpa(rs.getString(rs.getColumnIndex("ssc_gpa")));
            itemDB.setSsc_regi(rs.getString(rs.getColumnIndex("ssc_regi")));
            itemDB.setSsc_roll(rs.getString(rs.getColumnIndex("ssc_roll")));
            itemDB.setHsc_year(rs.getString(rs.getColumnIndex("hsc_year")));
            itemDB.setHsc_board(rs.getString(rs.getColumnIndex("hsc_board")));
            itemDB.setHsc_gpa(rs.getString(rs.getColumnIndex("hsc_gpa")));
            itemDB.setHsc_regi(rs.getString(rs.getColumnIndex("hsc_regi")));
            itemDB.setHsc_roll(rs.getString(rs.getColumnIndex("hsc_roll")));
            itemDB.setPpUrl(rs.getString(rs.getColumnIndex("ppUrl")));
            itemDB.setSignatureUrl(rs.getString(rs.getColumnIndex("signatureUrl")));

            arraylist.add(itemDB);

            rs.moveToNext();
        }
        return arraylist;
    }

    public ProfileItem getProfile(String id) {
        ProfileItem itemDB = new ProfileItem();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor rs = db.rawQuery("select * from "+TABLE_NAME+" WHERE user_id = '"+id+"'", null);
        rs.moveToFirst();
        while (!rs.isAfterLast()) {


            itemDB.setId(rs.getString(rs.getColumnIndex("user_id")));
            itemDB.setName(rs.getString(rs.getColumnIndex("name")));
            itemDB.setPhone(rs.getString(rs.getColumnIndex("phone")));
            itemDB.setPassword(rs.getString(rs.getColumnIndex("password")));
            itemDB.setApiKey(rs.getString(rs.getColumnIndex("api_key")));
            itemDB.setSsc_year(rs.getString(rs.getColumnIndex("ssc_year")));
            itemDB.setSsc_board(rs.getString(rs.getColumnIndex("ssc_board")));
            itemDB.setSsc_gpa(rs.getString(rs.getColumnIndex("ssc_gpa")));
            itemDB.setSsc_regi(rs.getString(rs.getColumnIndex("ssc_regi")));
            itemDB.setSsc_roll(rs.getString(rs.getColumnIndex("ssc_roll")));
            itemDB.setHsc_year(rs.getString(rs.getColumnIndex("hsc_year")));
            itemDB.setHsc_board(rs.getString(rs.getColumnIndex("hsc_board")));
            itemDB.setHsc_gpa(rs.getString(rs.getColumnIndex("hsc_gpa")));
            itemDB.setHsc_regi(rs.getString(rs.getColumnIndex("hsc_regi")));
            itemDB.setHsc_roll(rs.getString(rs.getColumnIndex("hsc_roll")));
            itemDB.setPpUrl(rs.getString(rs.getColumnIndex("ppUrl")));
            itemDB.setSignatureUrl(rs.getString(rs.getColumnIndex("signatureUrl")));

            rs.moveToNext();
        }
        return itemDB;
    }


    public Integer Delete(String taskId) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "user_id = ?", new String[]{String.valueOf(taskId)});
    }

    public void DeleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_NAME);
    }


}

