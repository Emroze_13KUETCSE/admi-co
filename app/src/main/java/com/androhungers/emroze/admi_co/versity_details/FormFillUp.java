package com.androhungers.emroze.admi_co.versity_details;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androhungers.emroze.admi_co.CustomDialog;
import com.androhungers.emroze.admi_co.R;
import com.androhungers.emroze.admi_co.form.FormListActivity;
import com.androhungers.emroze.admi_co.profile.DialogProfileAdapter;
import com.androhungers.emroze.admi_co.profile.ProfileDB;
import com.androhungers.emroze.admi_co.profile.ProfileDetails;
import com.androhungers.emroze.admi_co.profile.ProfileItem;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FormFillUp extends AppCompatActivity {

    public static String PREFS_NAME = "Admi-Co";
    SharedPreferences preference;
    SharedPreferences.Editor editor;
    String baseUrl;
    RequestQueue requestQueue;
    RelativeLayout view;


    Dialog dialogView;
    CustomDialog customDialog;

    Dialog dialogView2;
    CustomDialog customDialog2;

    RecyclerView profileList;
    ProfileDB profileDB;

    ArrayList<ProfileItem> profileItems = new ArrayList<>();
    DialogProfileAdapter dialogProfileAdapter;

    RecyclerView recyclerView;
    ArrayList<UnitItem> unitItems = new ArrayList<>();
    UnitAdapter_form unitAdapter_form;
    ArrayList<VersityItem> versityItemArrayList = new ArrayList<>();

    String response,name;

    TextView tvPrice,tvName,tvPhone, tvVersityName,tvConfirm;

    ImageView imgPP,imgSign;

    ProfileItem profileItemDB,profileItem;
    TextView etSSC_gpa,etSSC_board,etSSC_year,etSSC_roll,etSSC_regi,etHSC_gpa,etHSC_board,etHSC_year,etHSC_roll,etHSC_regi;

    /*
     * $verify = verifyRequiredParams(array('versity_name', 'has_unit', 'unit_name', 'student_id', 'student_number', 'status', 'payment_amount'));

     */

    String versity_name,unit_name,student_id,student_number,status,payment_amount,versity_id;

    private void init(){
        tvPrice = findViewById(R.id.tv_form_price);
        tvName = findViewById(R.id.tv_name);
        tvPhone = findViewById(R.id.tv_phone);
        tvConfirm = findViewById(R.id.tv_confirm);

        etSSC_gpa = findViewById(R.id.et_ssc_gpa);
        etSSC_board = findViewById(R.id.et_ssc_board);
        etSSC_year = findViewById(R.id.et_ssc_year);
        etSSC_regi = findViewById(R.id.et_ssc_regi);
        etSSC_roll = findViewById(R.id.et_ssc_roll);
        etHSC_board = findViewById(R.id.et_hsc_board);
        etHSC_gpa = findViewById(R.id.et_hsc_gpa);
        etHSC_regi = findViewById(R.id.et_hsc_regi);
        etHSC_roll = findViewById(R.id.et_hsc_roll);
        etHSC_year = findViewById(R.id.et_hsc_year);

        tvVersityName = findViewById(R.id.tv_versity_name);



        imgPP = findViewById(R.id.img_pp);
        imgSign = findViewById(R.id.img_sign);

    }

    private void setProfileData(ProfileItem profileItemDB){

        tvName.setText(profileItemDB.getName());
        tvPhone.setText(profileItemDB.getPhone());
        etSSC_board.setText(profileItemDB.getSsc_board());
        etSSC_gpa.setText(profileItemDB.getSsc_gpa());
        etSSC_regi.setText(profileItemDB.getSsc_regi());
        etSSC_roll.setText(profileItemDB.getSsc_roll());
        etSSC_year.setText(profileItemDB.getSsc_year());

        etHSC_board.setText(profileItemDB.getHsc_board());
        etHSC_gpa.setText(profileItemDB.getHsc_gpa());
        etHSC_regi.setText(profileItemDB.getHsc_regi());
        etHSC_roll.setText(profileItemDB.getHsc_roll());
        etHSC_year.setText(profileItemDB.getHsc_year());

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_fill_up);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        init();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Form Details");

        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        System.out.println(preference.getString("api_key",""));
        baseUrl = getString(R.string.base_url);
        requestQueue = Volley.newRequestQueue(this);

        view = findViewById(R.id.view);
        view.setVisibility(View.GONE);


        name = getIntent().getStringExtra("name");
        response = getIntent().getStringExtra("data");
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(response);

            for(int i =0 ; i < jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String id = jsonObject.getString("id");
                String name = jsonObject.getString("name");

                if(name.equals(this.name)){
                    String details = jsonObject.getString("details");
                    String has_units = jsonObject.getString("has_units");
                    String unit_details = jsonObject.getString("unit_details");
                    String form_fill_up = jsonObject.getString("form_fill_up");
                    String exam = jsonObject.getString("exam");
                    String result = jsonObject.getString("result");

                    VersityItem profileItem = new VersityItem();

                    profileItem.setId(id);
                    profileItem.setName(name);
                    profileItem.setDetails(details);
                    profileItem.setHas_units(has_units);
                    profileItem.setUnit_details(unit_details);
                    profileItem.setForm_fill_up(form_fill_up);
                    profileItem.setExam(exam);
                    profileItem.setResult(result);
                    profileItem.setFee(jsonObject.getString("fee"));
                    profileItem.setStatus(jsonObject.getString("status"));
                    versityItemArrayList.add(profileItem);

                    UnitItem unitItem = new UnitItem(has_units,false);
                    if(unitItems.isEmpty()){
                        unitItem.setSelected(true);
                    }


                    JSONArray jsonArray1 = new JSONArray(unit_details);
                    ArrayList<DetailsItem> detailsItems = new ArrayList<>();

                    for(int j = 0; j < jsonArray1.length(); j++){
                        JSONObject jj = jsonArray1.getJSONObject(j);
                        detailsItems.add(new DetailsItem(jj.getString("value"),jj.getString("data"),false));
                    }

                    unitItem.setDetailsItems(detailsItems);
                    unitItem.setFee(jsonObject.getString("fee"));
                    unitItems.add(unitItem);

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        recyclerView = findViewById(R.id.recycler_view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);


        profileDB = new ProfileDB(getApplicationContext());
        String id = preference.getString("id","");
        profileItemDB = profileDB.getProfile(id);

        unitAdapter_form = new UnitAdapter_form(unitItems,getApplicationContext());
        unitAdapter_form.setListener(listener);
        recyclerView.setAdapter(unitAdapter_form);

        tvPrice.setText("BDT "+versityItemArrayList.get(0).getFee());

        payment_amount = versityItemArrayList.get(0).getFee();
        versity_name = versityItemArrayList.get(0).getName();
        unit_name = unitItems.get(0).getName();
        versity_id = versityItemArrayList.get(0).getId();


        setProfileData(profileItemDB);

        RequestOptions requestOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                .skipMemoryCache(true);

        System.out.println(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png");
        Glide.with(getApplicationContext())
                .applyDefaultRequestOptions(requestOptions)
                .load(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_sign.png")
                .into(imgSign);

        RequestOptions requestOptions2 = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_pp)
                .error(R.mipmap.ic_pp)
                .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                .skipMemoryCache(true);

        System.out.println(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png");
        Glide.with(getApplicationContext())
                .applyDefaultRequestOptions(requestOptions2)
                .load(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png")
                .into(imgPP);

        tvVersityName.setText(versityItemArrayList.get(0).getName());


        customDialog2 = new CustomDialog();
        customDialog2.setDialog(FormFillUp.this,getApplicationContext(),R.layout.payment_dialog);
        dialogView2 = customDialog2.getView();

        dialogView2.findViewById(R.id.ll_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                student_id = preference.getString("id","");
                student_number = preference.getString("phone","");
                httpRequestForm(versity_name,"true",unit_name,student_id,student_number,"0",payment_amount,versity_id);
            }
        });
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView) dialogView2.findViewById(R.id.tv_amount);
                tv.setText("BDT "+payment_amount);
                customDialog2.showDialog();
            }
        });

    }

    UnitAdapter_form.OnCarItemClickListener listener = new UnitAdapter_form.OnCarItemClickListener() {
        @Override
        public void onllClick(int position, UnitItem item) {
            for(int i = 0 ; i < unitItems.size();i++){
                if(position == i){
                    unitItems.get(i).setSelected(true);
                }else {
                    unitItems.get(i).setSelected(false);
                }
            }

            payment_amount = item.getFee();
            unit_name = item.getName();
            versity_id = versityItemArrayList.get(position).getId();

            tvPrice.setText("BDT "+item.getFee());
            unitAdapter_form.setItemList(unitItems);
            unitAdapter_form.notifyDataSetChanged();
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.form_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_profile){
            profileItems = profileDB.getProfileList();

            customDialog = new CustomDialog();
            customDialog.setDialog(FormFillUp.this,getApplicationContext(),R.layout.profile_list_dialog);
            dialogView = customDialog.getView();

            profileList = dialogView.findViewById(R.id.recycler_view);
            profileList.setHasFixedSize(true);
            LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(FormFillUp.this, LinearLayoutManager.VERTICAL, false);
            profileList.setLayoutManager(horizontalLayoutManager);

            dialogProfileAdapter = new DialogProfileAdapter(profileItems,getApplicationContext());
            dialogProfileAdapter.setItemList(profileItems);
            profileList.setAdapter(dialogProfileAdapter);
            dialogProfileAdapter.setListener(dialogItemListern);
            dialogProfileAdapter.notifyDataSetChanged();

            customDialog.showDialog();

        }
        return super.onOptionsItemSelected(item);
    }

    DialogProfileAdapter.OnCarItemClickListener dialogItemListern = new DialogProfileAdapter.OnCarItemClickListener() {
        @Override
        public void onLongClick(int position, ProfileItem item) {
            Intent intent = new Intent(FormFillUp.this, ProfileDetails.class);
            startActivity(intent);
        }

        @Override
        public void onClick(int position, ProfileItem profileItem) {

            setProfileData(profileItem);

            editor.putString("api_key",profileItem.getApiKey());
            editor.putString("name",profileItem.getName());
            editor.putString("phone",profileItem.getPhone());
            editor.putString("id",profileItem.getId());
            editor.putString("sign_in","true");
            editor.commit();

            RequestOptions requestOptions = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                    .skipMemoryCache(true);

            System.out.println(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png");
            Glide.with(getApplicationContext())
                    .applyDefaultRequestOptions(requestOptions)
                    .load(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_sign.png")
                    .into(imgSign);

            RequestOptions requestOptions2 = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.mipmap.ic_pp)
                    .error(R.mipmap.ic_pp)
                    .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                    .skipMemoryCache(true);

            System.out.println(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png");
            Glide.with(getApplicationContext())
                    .applyDefaultRequestOptions(requestOptions2)
                    .load(baseUrl+"admi_co/v2/uploads/"+preference.getString("api_key","")+"_pp.png")
                    .into(imgPP);


            dialogProfileAdapter.notifyDataSetChanged();
        }
    };


    public void httpRequestForm(final String versity_name, final String has_unit, final String unit_name, final String student_id, final String student_number, final String status, final String payment_amount, final String versity_id){

        /*
         * $verify = verifyRequiredParams(array('versity_name', 'has_unit', 'unit_name', 'student_id', 'student_number', 'status', 'payment_amount'));

         */
        String url = baseUrl+"admi_co/v2/request_form";
        view.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        view.setVisibility(View.GONE);
                        Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  RESPONSE ====================    "+response);

                        Intent intent = new Intent(FormFillUp.this, FormListActivity.class);
                        startActivity(intent);
                        //customDialog2.hideDialog();
//                        customDialog.hideDialog();
                        finish();
                        //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        view.setVisibility(View.GONE);
                        Toast.makeText(FormFillUp.this, "Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", preference.getString("api_key",""));
                return params;
            }

            @Override
            protected Map<String, String> getParams()
            {
                /*
                * $verify = verifyRequiredParams(array('versity_name', 'has_unit', 'unit_name', 'student_id', 'student_number', 'status', 'payment_amount'));

                 */
                Map<String, String> params = new HashMap<String, String>();
                params.put("versity_name", versity_name);
                params.put("has_unit", has_unit);
                params.put("unit_name", unit_name);
                params.put("student_id", student_id);
                params.put("student_number", student_number);
                params.put("status", status);
                params.put("payment_amount", payment_amount);
                params.put("versity_id", versity_id);

                return params;
            }
            @Override
            protected com.android.volley.Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10*1000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        stringRequest.setShouldCache(false);

        requestQueue.add(stringRequest);


    }
}
