package com.androhungers.emroze.admi_co;

public class Constants {
    public static String PAYMENT_NOT_CONFIRM = "0";
    public static String TX_SUBMITTED = "1";
    public static String PAYMENT_CONFIRM = "2";
    public static String FORM_FILL_UP_OK = "3";
    public static String ADMIT_CARD_OK = "4";
}
