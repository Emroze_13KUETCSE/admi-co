package com.androhungers.emroze.admi_co;

/**
 * Created by emroze on 4/11/18.
 */

public class getData {
    String message;
    boolean success;

    public getData(String message, boolean success) {
        this.message = message;
        this.success = success;

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
