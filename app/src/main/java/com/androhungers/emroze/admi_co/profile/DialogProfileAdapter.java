package com.androhungers.emroze.admi_co.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androhungers.emroze.admi_co.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

public class DialogProfileAdapter extends RecyclerView.Adapter<DialogProfileAdapter.CarsViewHolder>{

    public static String PREFS_NAME = "Admi-Co";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    public ArrayList<ProfileItem> itemArrayList;
    Context context;
    private OnCarItemClickListener listener;

    public interface OnCarItemClickListener {

        /**
         * The method declaration for user selected. This method will be fired
         * when user click on check/uncheck the checkbox on the list item.
         *
         * @param position
         * @param item
         */
        public void onLongClick(int position, ProfileItem item);
        public void onClick(int position, ProfileItem item);


    }
    public void setListener(DialogProfileAdapter.OnCarItemClickListener listener) {
        this.listener = listener;
    }

    public DialogProfileAdapter(ArrayList<ProfileItem> items, Context context){
        this.itemArrayList= items;
        this.context = context;
        preference = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

    }

    public void setItemList(ArrayList<ProfileItem> list) {
        itemArrayList = list;
    }

    @Override
    public CarsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_list_item, parent, false);
        CarsViewHolder gvh = new CarsViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final DialogProfileAdapter.CarsViewHolder holder, final int position) {

        final ProfileItem item = itemArrayList.get(position);

        holder.name.setText(item.getName());
        holder.phone.setText(item.getPhone());

        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.man)
                .error(R.drawable.man)
                .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                .skipMemoryCache(true);

        String baseUrl = context.getString(R.string.base_url);

        Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(baseUrl+"admi_co/v2/uploads/"+item.getApiKey()+"_pp.png")
                .into(holder.profile);

        if(item.getId().equals(preference.getString("id",""))){
            holder.ll.setBackgroundColor(Color.parseColor("#E1F7F3"));
            holder.imgOk.setVisibility(View.VISIBLE);
        }else {
            holder.ll.setBackgroundColor(Color.parseColor("#FFFFFF"));
            holder.imgOk.setVisibility(View.GONE);
        }
        holder.ll.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onLongClick(position,item);
                return false;
            }
        });

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(position,item);
            }
        });


    }






    @Override
    public int getItemCount() {
        if (itemArrayList != null) {
            return itemArrayList.size();
        } else {
            return 0;
        }
    }

    public class CarsViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView phone;
        ImageView imgOk;
        CircleImageView profile;

        LinearLayout ll;


        public CarsViewHolder(View view) {
            super(view);


            ll = view.findViewById(R.id.ll);
            imgOk = view.findViewById(R.id.img_ok);

            name = (TextView) view.findViewById(R.id.tv_name);
            phone = (TextView) view.findViewById(R.id.tv_phone);

            profile =  view.findViewById(R.id.img_pro_pic);

            Typeface type1 = Typeface.createFromAsset(context.getAssets(),context.getString(R.string.font));
            Typeface type2 = Typeface.createFromAsset(context.getAssets(),context.getString(R.string.font));


            name.setTypeface(type1);
            phone.setTypeface(type1);


        }
    }
}
