package com.androhungers.emroze.admi_co.form;

public class FormItem {
    String id;
    String versityName;
    String unit;
    String stdId;
    String stdNumber;
    String stdPhone;
    String txId;
    String txPhone;
    String paymentAmount;
    String date;
    String status;
    String admitUrl;

    public String getAdmitUrl() {
        return admitUrl;
    }

    public void setAdmitUrl(String admitUrl) {
        this.admitUrl = admitUrl;
    }

    public FormItem(String id, String versityName, String unit, String stdId, String stdNumber, String stdPhone, String txId, String txPhone, String paymentAmount) {
        this.id = id;
        this.versityName = versityName;
        this.unit = unit;
        this.stdId = stdId;
        this.stdNumber = stdNumber;
        this.stdPhone = stdPhone;
        this.txId = txId;
        this.txPhone = txPhone;
        this.paymentAmount = paymentAmount;
    }

    public FormItem() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersityName() {
        return versityName;
    }

    public void setVersityName(String versityName) {
        this.versityName = versityName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStdId() {
        return stdId;
    }

    public void setStdId(String stdId) {
        this.stdId = stdId;
    }

    public String getStdNumber() {
        return stdNumber;
    }

    public void setStdNumber(String stdNumber) {
        this.stdNumber = stdNumber;
    }

    public String getStdPhone() {
        return stdPhone;
    }

    public void setStdPhone(String stdPhone) {
        this.stdPhone = stdPhone;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public String getTxPhone() {
        return txPhone;
    }

    public void setTxPhone(String txPhone) {
        this.txPhone = txPhone;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }
}
