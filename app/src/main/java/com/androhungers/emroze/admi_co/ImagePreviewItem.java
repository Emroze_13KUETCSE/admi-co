package com.androhungers.emroze.admi_co;

import android.graphics.Bitmap;

import java.io.File;

public class ImagePreviewItem {
    String fileName;
    Bitmap bitmap;
    File file;


    public ImagePreviewItem() {
    }

    public ImagePreviewItem(String fileName, Bitmap bitmap, File file) {
        this.fileName = fileName;
        this.bitmap = bitmap;
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
