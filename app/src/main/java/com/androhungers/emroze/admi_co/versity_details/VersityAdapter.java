package com.androhungers.emroze.admi_co.versity_details;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androhungers.emroze.admi_co.R;

import java.util.ArrayList;

public class VersityAdapter extends RecyclerView.Adapter<VersityAdapter.CarsViewHolder>{

    public ArrayList<VersityItem> itemArrayList;
    Context context;
    private OnCarItemClickListener listener;

    public interface OnCarItemClickListener {

        /**
         * The method declaration for user selected. This method will be fired
         * when user click on check/uncheck the checkbox on the list item.
         *
         * @param position
         * @param item
         */
        public void onLongClick(int position, VersityItem item);

    }
    public void setListener(VersityAdapter.OnCarItemClickListener listener) {
        this.listener = listener;
    }

    public VersityAdapter(ArrayList<VersityItem> items, Context context){
        this.itemArrayList= items;
        this.context = context;
    }

    public void setItemList(ArrayList<VersityItem> list) {
        itemArrayList = list;
    }

    @Override
    public CarsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.versity_item, parent, false);
        CarsViewHolder gvh = new CarsViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final VersityAdapter.CarsViewHolder holder, final int position) {

        final VersityItem item = itemArrayList.get(position);

        holder.name.setText(item.getName());
        holder.dates.setText(item.getForm_fill_up());

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onLongClick(position,item);
            }
        });

        holder.img.setImageResource(item.getResId());


    }

    @Override
    public int getItemCount() {
        if (itemArrayList != null) {
            return itemArrayList.size();
        } else {
            return 0;
        }
    }

    public class CarsViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView dates,tvDetails;
        ImageView img;

        RelativeLayout ll;


        public CarsViewHolder(View view) {
            super(view);


            ll = view.findViewById(R.id.rl);
            name = (TextView) view.findViewById(R.id.tv_versity_name);
            dates = (TextView) view.findViewById(R.id.tv_date);
            tvDetails = (TextView) view.findViewById(R.id.tv_details);


            img = (ImageView) view.findViewById(R.id.img);

            Typeface type1 = Typeface.createFromAsset(context.getAssets(),context.getString(R.string.font));
            Typeface type2 = Typeface.createFromAsset(context.getAssets(),context.getString(R.string.font));


            name.setTypeface(type1);
            dates.setTypeface(type1);


        }
    }
}

