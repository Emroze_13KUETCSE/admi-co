package com.androhungers.emroze.admi_co;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public class ApiConfig {

    public interface ApiInterface {
        @Multipart
        @POST("/admi_co/v2/upload")
        Call<getData> editUser (@Header("Authorization") String authorization, @Part("file\"; filename=\"pp.png\" ") RequestBody file );
    }

    public interface ApiInterface_photo {
        @Multipart
        @POST("/admi_co/v2/upload_pp")
        Call<getData> editUser (@Header("Authorization") String authorization, @Part("file\"; filename=\"pp.png\" ") RequestBody file, @Part("img_id") RequestBody img );
    }
}
