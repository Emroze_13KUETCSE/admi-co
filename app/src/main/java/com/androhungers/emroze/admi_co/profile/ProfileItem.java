package com.androhungers.emroze.admi_co.profile;

public class ProfileItem {
    private String id="";
    private String name="";
    private String phone="";
    private String password="";
    private String apiKey="";
    private String ssc_year="";
    private String ssc_board="";
    private String ssc_gpa="";
    private String ssc_regi="";
    private String ssc_roll="";
    private String hsc_year="";
    private String hsc_board="";
    private String hsc_gpa="";
    private String hsc_regi="";
    private String hsc_roll="";
    private String ppUrl="";
    private String signatureUrl="";


    public ProfileItem() {
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSsc_year() {
        return ssc_year;
    }

    public void setSsc_year(String ssc_year) {
        this.ssc_year = ssc_year;
    }

    public String getSsc_board() {
        return ssc_board;
    }

    public void setSsc_board(String ssc_board) {
        this.ssc_board = ssc_board;
    }

    public String getSsc_gpa() {
        return ssc_gpa;
    }

    public void setSsc_gpa(String ssc_gpa) {
        this.ssc_gpa = ssc_gpa;
    }

    public String getSsc_regi() {
        return ssc_regi;
    }

    public void setSsc_regi(String ssc_regi) {
        this.ssc_regi = ssc_regi;
    }

    public String getSsc_roll() {
        return ssc_roll;
    }

    public void setSsc_roll(String ssc_roll) {
        this.ssc_roll = ssc_roll;
    }

    public String getHsc_year() {
        return hsc_year;
    }

    public void setHsc_year(String hsc_year) {
        this.hsc_year = hsc_year;
    }

    public String getHsc_board() {
        return hsc_board;
    }

    public void setHsc_board(String hsc_board) {
        this.hsc_board = hsc_board;
    }

    public String getHsc_gpa() {
        return hsc_gpa;
    }

    public void setHsc_gpa(String hsc_gpa) {
        this.hsc_gpa = hsc_gpa;
    }

    public String getHsc_regi() {
        return hsc_regi;
    }

    public void setHsc_regi(String hsc_regi) {
        this.hsc_regi = hsc_regi;
    }

    public String getHsc_roll() {
        return hsc_roll;
    }

    public void setHsc_roll(String hsc_roll) {
        this.hsc_roll = hsc_roll;
    }

    public String getPpUrl() {
        return ppUrl;
    }

    public void setPpUrl(String ppUrl) {
        this.ppUrl = ppUrl;
    }

    public String getSignatureUrl() {
        return signatureUrl;
    }

    public void setSignatureUrl(String signatureUrl) {
        this.signatureUrl = signatureUrl;
    }
}
