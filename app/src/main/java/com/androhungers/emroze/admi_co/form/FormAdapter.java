package com.androhungers.emroze.admi_co.form;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androhungers.emroze.admi_co.R;
import com.androhungers.emroze.admi_co.versity_details.FormFillUp;
import com.androhungers.emroze.admi_co.versity_details.VersityItem;

import java.util.ArrayList;

import static com.androhungers.emroze.admi_co.Constants.ADMIT_CARD_OK;
import static com.androhungers.emroze.admi_co.Constants.FORM_FILL_UP_OK;
import static com.androhungers.emroze.admi_co.Constants.PAYMENT_CONFIRM;
import static com.androhungers.emroze.admi_co.Constants.PAYMENT_NOT_CONFIRM;
import static com.androhungers.emroze.admi_co.Constants.TX_SUBMITTED;

public class FormAdapter extends RecyclerView.Adapter<FormAdapter.CarsViewHolder>{

    public ArrayList<FormItem> itemArrayList;
    Context context;
    private OnCarItemClickListener listener;

    public interface OnCarItemClickListener {

        /**
         * The method declaration for user selected. This method will be fired
         * when user click on check/uncheck the checkbox on the list item.
         *
         * @param position
         * @param item
         */
        public void onLongClick(int position, FormItem item);

    }
    public void setListener(FormAdapter.OnCarItemClickListener listener) {
        this.listener = listener;
    }

    public FormAdapter(ArrayList<FormItem> items, Context context){
        this.itemArrayList= items;
        this.context = context;
    }

    public void setItemList(ArrayList<FormItem> list) {
        itemArrayList = list;
    }

    @Override
    public CarsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.form_list_item, parent, false);
        CarsViewHolder gvh = new CarsViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final FormAdapter.CarsViewHolder holder, final int position) {

        final FormItem item = itemArrayList.get(position);

        holder.vName.setText(item.getVersityName());
        holder.dates.setText(item.getDate());
        holder.tvUnit.setText(item.getUnit());
        holder.tvPayment.setText("BDT "+item.getPaymentAmount());

        String statusStr = "";
        String status="";

        if(item.getStatus().equals(PAYMENT_NOT_CONFIRM)){
            status = "Enter TxId";
            statusStr = "Payment not confirm";
        }
        else if(item.getStatus().equals(TX_SUBMITTED) ){
            status = "TxId Submitted";
            statusStr = "TxId Number is Submitted";
        }
        else if(item.getStatus().equals(PAYMENT_CONFIRM) ){
            status = "Payment ok";
            statusStr = "Payment is Ok";
        }
        else if(item.getStatus().equals(FORM_FILL_UP_OK)){
            status = "Ok";
            statusStr = "Form-fill up is Ok";
        }
        else if(item.getStatus().equals(ADMIT_CARD_OK)){
            status = "Admit Card";
            statusStr = "Admit card is avialable";
        }

        holder.tvOk.setText(status);
        holder.tvStatus.setText(statusStr);


        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onLongClick(position,item);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (itemArrayList != null) {
            return itemArrayList.size();
        } else {
            return 0;
        }
    }

    public class CarsViewHolder extends RecyclerView.ViewHolder {
        TextView vName,tvUnit,tvPayment,tvStatus,tvOk;
        TextView dates;
        ImageView img;

        RelativeLayout ll;


        public CarsViewHolder(View view) {
            super(view);


            ll = view.findViewById(R.id.rl);
            vName = (TextView) view.findViewById(R.id.tv_versity_name);
            dates = (TextView) view.findViewById(R.id.tv_date);
            tvUnit = (TextView) view.findViewById(R.id.tv_unit);
            tvPayment = (TextView) view.findViewById(R.id.tv_taka);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvOk = (TextView) view.findViewById(R.id.tv_ok);


            img = (ImageView) view.findViewById(R.id.img);

            Typeface type1 = Typeface.createFromAsset(context.getAssets(),context.getString(R.string.font));
            Typeface type2 = Typeface.createFromAsset(context.getAssets(),context.getString(R.string.font));



            dates.setTypeface(type1);


        }
    }
}

