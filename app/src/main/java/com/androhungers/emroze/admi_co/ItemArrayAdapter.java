package com.androhungers.emroze.admi_co;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.SwitchCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androhungers.emroze.admi_co.profile.ProfileDetails;

public class ItemArrayAdapter extends ArrayAdapter<String> {
    private DrawerLayout drawer;
    String[] itemList;
    private int listItemLayout;
    Context context;
    public ItemArrayAdapter(Context context, int layoutId, String[] itemList, DrawerLayout drawer) {
        super(context, layoutId, itemList);
        this.context = context;
        listItemLayout = layoutId;
        this.itemList = itemList;
        this.drawer = drawer;
    }

    String [] numbers = {"Helpline" ,"Profile", "About Us","Information"};

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int pos = position;
        final String item = getItem(pos);

        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(listItemLayout, parent, false);
            viewHolder.item = (TextView) convertView.findViewById(R.id.tv_text);
            viewHolder.img = (ImageView) convertView.findViewById(R.id.img);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if(position == 0){
            viewHolder.img.setImageResource(R.drawable.support);

            //hideMeSwitch.setChecked(true);
        }else if(position == 1){
            viewHolder.img.setImageResource(R.drawable.technical_support);

            //hideMeSwitch.setChecked(true);
        }else if(position == 2){
            viewHolder.img.setImageResource(R.drawable.innovation);

            //hideMeSwitch.setChecked(true);
        } else if(position == 3){
            viewHolder.img.setImageResource(R.drawable.innovation);

            //hideMeSwitch.setChecked(true);
        }

        viewHolder.item.setText(item);
        viewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewHolder.item.getText().toString().equals("Profile")){

                    Intent intent = new Intent(context, ProfileDetails.class);
                    context.startActivity(intent);
                }
                if(viewHolder.item.getText().toString().equals("Information")){

                    Intent intent = new Intent(context, ProfileDetails.class);
                    context.startActivity(intent);
                }


                drawer.closeDrawer(Gravity.LEFT);
            }
        });
        return convertView;
    }
    class ViewHolder {
        TextView item;
        SwitchCompat switchCompat;
        ImageView img;
    }
}