package com.androhungers.emroze.admi_co.signIn;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androhungers.emroze.admi_co.ApiConfig;
import com.androhungers.emroze.admi_co.ImagePreviewItem;
import com.androhungers.emroze.admi_co.MainActivity;
import com.androhungers.emroze.admi_co.R;
import com.androhungers.emroze.admi_co.getData;
import com.androhungers.emroze.admi_co.profile.ProfileDB;
import com.androhungers.emroze.admi_co.profile.ProfileItem;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateAccount extends AppCompatActivity {
    public static String PREFS_NAME = "Admi-Co";
    SharedPreferences preference;
    SharedPreferences.Editor editor;
    String baseUrl;
    RequestQueue requestQueue;
    RelativeLayout view;


    LinearLayout llAddpp;

    ImageView imgAdd,imgPreview,imgsign;

    Bitmap bitmapPP,bitmapSign;

    int state=0;

    TextView tvSign;

    EditText etName,etPhone,etPass,etSSC_gpa,etSSC_board,etSSC_year,etSSC_roll,etSSC_regi,etHSC_gpa,etHSC_board,etHSC_year,etHSC_roll,etHSC_regi;
    ProfileItem profileItem;

    ProfileDB profileDB;
    int uploadCounter = 0;

    ArrayList<ImagePreviewItem> imagePreviewItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Registration");



        profileDB = new ProfileDB(getApplicationContext());

        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        System.out.println(preference.getString("api_key",""));
        baseUrl = getString(R.string.base_url);
        requestQueue = Volley.newRequestQueue(this);

        view = findViewById(R.id.view);
        view.setVisibility(View.GONE);

        llAddpp = findViewById(R.id.ll_add);
        imgAdd = findViewById(R.id.img_add);
        imgPreview = findViewById(R.id.img_preview);
        tvSign = findViewById(R.id.tv_sign);
        imgsign = findViewById(R.id.img_sign);

        etName = findViewById(R.id.et_name);
        etPhone = findViewById(R.id.et_phone);
        etPass = findViewById(R.id.et_pass);
        etSSC_gpa = findViewById(R.id.et_ssc_gpa);
        etSSC_board = findViewById(R.id.et_ssc_board);
        etSSC_year = findViewById(R.id.et_ssc_year);
        etSSC_regi = findViewById(R.id.et_ssc_regi);
        etSSC_roll = findViewById(R.id.et_ssc_roll);
        etHSC_board = findViewById(R.id.et_hsc_board);
        etHSC_gpa = findViewById(R.id.et_hsc_gpa);
        etHSC_regi = findViewById(R.id.et_hsc_regi);
        etHSC_roll = findViewById(R.id.et_hsc_roll);
        etHSC_year = findViewById(R.id.et_hsc_year);


        llAddpp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                state = 0;
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(CreateAccount.this);
            }
        });

        tvSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                state = 1;
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(CreateAccount.this);
            }
        });

        ImagePreviewItem item1 = new ImagePreviewItem("PP",bitmapPP,new File(""));

        ImagePreviewItem item2 = new ImagePreviewItem("Sign",bitmapPP,new File(""));

        imagePreviewItems.add(item1);
        imagePreviewItems.add(item2);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.ok_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_ok){
            profileItem = new ProfileItem();
            profileItem.setName(etName.getText().toString());
            profileItem.setPhone(etPhone.getText().toString());
            profileItem.setPassword(etPass.getText().toString());
            profileItem.setPpUrl("aaa");
            profileItem.setSignatureUrl("aaaa");
            profileItem.setSsc_board(etSSC_board.getText().toString());
            profileItem.setSsc_gpa(etSSC_gpa.getText().toString());
            profileItem.setSsc_regi(etSSC_regi.getText().toString());
            profileItem.setSsc_roll(etSSC_roll.getText().toString());
            profileItem.setSsc_year(etSSC_year.getText().toString());
            profileItem.setHsc_board(etHSC_board.getText().toString());
            profileItem.setHsc_gpa(etHSC_gpa.getText().toString());
            profileItem.setHsc_regi(etHSC_regi.getText().toString());
            profileItem.setHsc_roll(etHSC_roll.getText().toString());
            profileItem.setHsc_year(etHSC_year.getText().toString());

            if(profileItem.getName().equals("") ||
                    profileItem.getPassword().equals("")||
                    profileItem.getPhone().equals("")||
                    profileItem.getPpUrl().equals("")||
                    profileItem.getSignatureUrl().equals("")||
                    profileItem.getSsc_board().equals("")||
                    profileItem.getSsc_gpa().equals("")||
                    profileItem.getSsc_regi().equals("")||
                    profileItem.getSsc_roll().equals("")||
                    profileItem.getSsc_year().equals("")||
                    profileItem.getHsc_board().equals("")||
                    profileItem.getHsc_gpa().equals("")||
                    profileItem.getHsc_regi().equals("")||
                    profileItem.getHsc_roll().equals("")||
                    profileItem.getHsc_year().equals("")
            ){
                Toast.makeText(this, "Please enter the form properly", Toast.LENGTH_SHORT).show();
            }
            else {
                JSONObject jsonObject_ssc = new JSONObject();
                try {
                    jsonObject_ssc.put("board",profileItem.getSsc_board());
                    jsonObject_ssc.put("gpa",profileItem.getSsc_gpa());
                    jsonObject_ssc.put("regi",profileItem.getSsc_regi());
                    jsonObject_ssc.put("roll",profileItem.getSsc_roll());
                    jsonObject_ssc.put("year",profileItem.getSsc_year());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JSONObject jsonObject_hsc = new JSONObject();
                try {
                    jsonObject_hsc.put("board",profileItem.getHsc_board());
                    jsonObject_hsc.put("gpa",profileItem.getHsc_gpa());
                    jsonObject_hsc.put("regi",profileItem.getHsc_regi());
                    jsonObject_hsc.put("roll",profileItem.getHsc_roll());
                    jsonObject_hsc.put("year",profileItem.getHsc_year());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                httpRegister(profileItem.getName(),profileItem.getPhone(),profileItem.getPassword(),jsonObject_ssc.toString(),jsonObject_hsc.toString(),profileItem.getPpUrl(),profileItem.getSignatureUrl());
            }

        }
        return super.onOptionsItemSelected(item);
    }

    class FileImageProcess extends AsyncTask<Void,Void,Void> {
        File compressedImage;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {



            try {
                //bitmap = new Compressor(Profile.this).compressToBitmap(inputFile);
                compressedImage = new Compressor(CreateAccount.this)
                        .setQuality(90)
                        .setCompressFormat(Bitmap.CompressFormat.JPEG)
                        .compressToFile(imagePreviewItems.get(uploadCounter).getFile());

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            imagePreviewItems.get(uploadCounter).setFile(compressedImage);

            if(uploadCounter == 0){
                httpUploadPic(preference.getString("api_key","")+"_pp",imagePreviewItems.get(uploadCounter).getFile());
            }else {
                httpUploadPic(preference.getString("api_key","")+"_sign",imagePreviewItems.get(uploadCounter).getFile());
            }

            //System.out.println("Size >>>>>>>>>>>>>>>>>>>>>>>  "+String.format("Size : %s", getReadableFileSize(compressedImage.length())));
        }
    }

    public void httpUploadPic(String imgId , File file){

        view.setVisibility(View.VISIBLE);
        Retrofit.Builder builder2 = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit2 = builder2.build();

        final ApiConfig.ApiInterface_photo eee = retrofit2.create(ApiConfig.ApiInterface_photo.class);

        RequestBody fbody = RequestBody.create(MediaType.parse("image/*"), file);
        RequestBody id = RequestBody.create(MediaType.parse("multipart/form-data"), imgId);

        Call<getData> call2 = eee.editUser(preference.getString("api_key",""), fbody,id);
        call2.enqueue(new Callback<getData>() {
            @Override
            public void onResponse(Call<getData> call, Response<getData> response) {
                System.out.println(response.body().getMessage());
                if(response.body().isSuccess()){

                    ++uploadCounter;
                    if(uploadCounter < imagePreviewItems.size()){

                        new FileImageProcess().execute();
                    }
                    else {
                        finish();
                        Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        //httpRegister(profileItem.getName(),profileItem.getPhone(),profileItem.getPassword(),jsonObject_ssc.toString(),jsonObject_hsc.toString(),profileItem.getPpUrl(),profileItem.getSignatureUrl());
                    }

                }
            }

            @Override
            public void onFailure(Call<getData> call, Throwable t) {
                System.out.println("emmii >>>> fdjkhfkdjhfkjhsdfkjhsdfkjsdkfjs "+t.toString());
                //finish();
            }
        });

    }


    public void httpRegister(final String name, final String phone, final String pass, final String ssc, final String hsc, final String ppUrl, final String signUrl){

        String url = baseUrl+"admi_co/v2/register_student";
        view.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        view.setVisibility(View.GONE);
                        Log.d("CODE>>>>>>>>>>>>>>>>>"," =================  RESPONSE ====================    "+response);
                        try {
                            JSONObject object = new JSONObject(response);
                            boolean ss = object.getBoolean("success");
                            if(ss){
                                profileItem.setId(object.getJSONObject("data").getString("id"));
                                profileItem.setApiKey(object.getJSONObject("data").getString("api_key"));

                                editor.putString("api_key",object.getJSONObject("data").getString("api_key"));
                                editor.putString("name",profileItem.getName());
                                editor.putString("phone",profileItem.getPhone());
                                editor.putString("id",profileItem.getId());
                                editor.putString("sign_in","true");
                                editor.commit();

                                profileDB.insertToProfileList(profileItem);

                                uploadCounter = 0;
                                if(!imagePreviewItems.isEmpty()){
                                    new FileImageProcess().execute();
                                }
                                else {
                                    //httpRegister(profileItem.getName(),profileItem.getPhone(),profileItem.getPassword(),jsonObject_ssc.toString(),jsonObject_hsc.toString(),profileItem.getPpUrl(),profileItem.getSignatureUrl());

                                    finish();
                                    Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }


                            }
                            else {
                                Toast.makeText(CreateAccount.this, "Please try again", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        view.setVisibility(View.GONE);
                        Toast.makeText(CreateAccount.this, "Please try again. No Internet", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", preference.getString("api_key",""));
                return params;
            }

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("phone", phone);
                params.put("password", pass);
                params.put("pp_url", ppUrl);
                params.put("sign_url", signUrl);
                params.put("ssc", ssc);
                params.put("hsc", hsc);

                return params;
            }
            @Override
            protected com.android.volley.Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10*1000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        stringRequest.setShouldCache(false);

        requestQueue.add(stringRequest);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Bitmap bitmap = null;

                System.out.println(">>>>>>>>>>>>>>>>>>>> ok file pick ");

                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                    if(state == 0){
                        imgPreview.setVisibility(View.VISIBLE);
                        imgPreview.setImageBitmap(bitmap);
                        bitmapPP = bitmap;
                        String name = preference.getString("api_key","")+"_pp";
                        ImagePreviewItem item = new ImagePreviewItem(name,bitmapPP,new File(resultUri.getPath()));


                        imagePreviewItems.add(0,item);

                    }else {
                        imgsign.setImageBitmap(bitmap);
                        bitmapSign = bitmap;

                        String name = preference.getString("api_key","")+"_sign";
                        imagePreviewItems.add(1,new ImagePreviewItem(name,bitmapSign,new File(resultUri.getPath())));

                    }



                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                System.out.println("only result 2"+error.toString());
            }
        }
    }

}
